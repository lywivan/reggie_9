# 瑞尔外卖项目代码

#### 介绍
瑞尔点餐

#### 软件架构
基于SSM+Spirngboot的点餐送餐项目


#### 软件架构

后端主要技术：

ssm+springboot进行业务开发

springsession实现分布式session共享

yapi+swagger2进行接口api文档管理

nginx实现tomcat服务器的负载均衡

mysql主从集群作为数据库

shardingjdbc完成数据的读写分离以及分库分表

mybatis+mybaits plus操作数据库

redis+speingcache对数据进行缓存

git进行分布式版本管理

maven进行项目管理

采用支付宝开放平台实现在线支付功能

采用阿里云sms完成短信业务的开发

前端技术：

vue+element ui+axios


#### 成品效果网站

由于项目采用支付宝的沙箱模式，支付时采用测试账号。

账号：bfxhjn7771@sandbox.com

密码：111111

支付密码：111111




前台页面：http://www.lywivan.top:81/front/page/login.html

由于阿里云中余额有限，短信验证码建议从页面f12的响应结果中获取




后台页面：http://www.lywivan.top:81/backend/page/login/login.html

后台系统管理员账号：admin   密码：123456

后台系统普通员工账号：zhangsan  密码：123456
