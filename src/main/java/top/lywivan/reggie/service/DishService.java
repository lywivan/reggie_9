package top.lywivan.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;

public interface DishService extends IService<Dish> {
    void saveWithFlavors(DishDto dishDto);

    DishDto getByIdWithDishFlavor(Long id);

    void updateWithDishFlavor(DishDto dishDto);

    void removeWithDishFlavor(Long[] ids);
}
