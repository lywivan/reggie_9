package top.lywivan.reggie.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.Constants;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import top.lywivan.reggie.exception.BusinessException;
import top.lywivan.reggie.mapper.SetmealMapper;
import com.itheima.reggie.service.DishService;
import top.lywivan.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {
    @Autowired
    private SetmealDishService setmealDishService;
    @Autowired
    private DishService dishService;
    @Override
    public void saveWithSetmealDish(SetmealDto setmealDto) {
        this.save(setmealDto);
        List<SetmealDish> setmealDishList = setmealDto.getSetmealDishes();
        List<SetmealDish> setmealDishes = setmealDishList.stream().peek(setmealDish -> setmealDish.setSetmealId(setmealDto.getId())).collect(Collectors.toList());
        setmealDishService.saveBatch(setmealDishes);
    }

    @Override
    public void deleteWithSetmealDish(Long[] ids) {
        List<Setmeal> setmealList = this.list(Wrappers.<Setmeal>lambdaQuery().in(Setmeal::getId, (Object[]) ids));
        setmealList.forEach(setmeal -> {
            if (Constants.SETMEAL_ON_SALE.equals(setmeal.getStatus())){
                throw new BusinessException("请先下架相关套餐");
            }
        });
        this.remove(Wrappers.<Setmeal>lambdaQuery().in(Setmeal::getId, (Object[]) ids));
        setmealDishService.remove(Wrappers.<SetmealDish>lambdaQuery().in(SetmealDish::getSetmealId, (Object[]) ids));
    }

    @Override
    public SetmealDto getByIdWithSetmealDish(Long id) {
        Setmeal setmeal = this.getById(id);
        SetmealDto setmealDto = new SetmealDto();
        BeanUtils.copyProperties(setmeal,setmealDto);
        List<SetmealDish> setmealDishList = setmealDishService.list(Wrappers.<SetmealDish>lambdaQuery().eq(SetmealDish::getSetmealId, id));
        setmealDto.setSetmealDishes(setmealDishList);
        return setmealDto;
    }

    @Override
    public void updateWithSetmealDish(SetmealDto setmealDto) {
        this.updateById(setmealDto);
        setmealDishService.remove(Wrappers.<SetmealDish>lambdaQuery().eq(SetmealDish::getSetmealId,setmealDto.getId()));
        List<SetmealDish> setmealDishList = setmealDto.getSetmealDishes();
        List<SetmealDish> setmealDishes = setmealDishList.stream().peek(setmealDish -> setmealDish.setSetmealId(setmealDto.getId())).collect(Collectors.toList());
        setmealDishService.saveBatch(setmealDishes);
    }

    @Override
    public List<DishDto> getDishBySetmeal(Long setmealId) {
        List<SetmealDish> setmealDishList = setmealDishService.list(Wrappers.<SetmealDish>lambdaQuery().eq(SetmealDish::getSetmealId, setmealId));
        return setmealDishList.stream().map(setmealDish ->  {
            Dish dish = dishService.getById(setmealDish.getDishId());
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(dish,dishDto);
            dishDto.setCopies(setmealDish.getCopies());
            return dishDto;
        }).collect(Collectors.toList());
    }
}
