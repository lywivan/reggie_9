package top.lywivan.reggie.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import top.lywivan.reggie.exception.BusinessException;
import top.lywivan.reggie.mapper.CategoryMapper;
import top.lywivan.reggie.service.CategoryService;
import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Autowired
    private DishService dishService;
    @Autowired
    private SetmealService setmealService;

    /**
     * 用于根据id删除分类，但删除前判断是否有关联内容
     * @param id 要删除的分类的id
     */
    @Override
    public void remove(Long id) {
        int count4Dish = dishService.count(Wrappers.<Dish>lambdaQuery().eq(Dish::getCategoryId, id));
        if (count4Dish>0){
            throw new BusinessException("有"+count4Dish+"条关联的菜品，无法直接删除");
        }
        int count4Setmeal = setmealService.count(Wrappers.<Setmeal>lambdaQuery().eq(Setmeal::getCategoryId, id));
        if (count4Setmeal>0){
            throw new BusinessException("有"+count4Setmeal+"条关联的套餐，无法直接删除");
        }
        this.removeById(id);
    }
}
