package top.lywivan.reggie.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.Constants;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import top.lywivan.reggie.exception.BusinessException;
import top.lywivan.reggie.mapper.DishMapper;
import top.lywivan.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
    @Autowired
    private DishFlavorService dishFlavorService;

    /**
     * 实现添加菜品同时添加口味
     * @param dishDto
     */
    @Override
    @Transactional
    public void saveWithFlavors(DishDto dishDto) {
        //先保存基本信息
        this.save(dishDto);
        List<DishFlavor> flavorList = dishDto.getFlavors();
        //利用stream流设置每一个口味的菜品id
        List<DishFlavor> dishFlavorListWithDishId = flavorList.stream().peek(dishFlavor -> dishFlavor.setDishId(dishDto.getId())).collect(Collectors.toList());
        dishFlavorService.saveBatch(dishFlavorListWithDishId);
    }

    /**
     * 根据id查询菜品对象，用于数据回显，需要查询出相关的口味信息，因此需要使用Dto封装后返回
     * @param id 要查询的菜品id
     * @return
     */
    @Override
    public DishDto getByIdWithDishFlavor(Long id) {
        Dish dish = this.getById(id);
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish,dishDto);
        List<DishFlavor> dishFlavorList = dishFlavorService.list(Wrappers.<DishFlavor>lambdaQuery().eq(DishFlavor::getDishId, id));
        dishDto.setFlavors(dishFlavorList);
        return dishDto;
    }

    @Override
    @Transactional
    public void updateWithDishFlavor(DishDto dishDto) {
        //先修改基本信息
        this.updateById(dishDto);
        //修改口味信息时，先将原来的口味删除，再添加新口味
        dishFlavorService.remove(Wrappers.<DishFlavor>lambdaQuery().eq(DishFlavor::getDishId,dishDto.getId()));
        //新增的口味需要绑定对应的菜品id
        List<DishFlavor> dishFlavorList = dishDto.getFlavors().stream().peek(dishFlavor -> dishFlavor.setDishId(dishDto.getId())).collect(Collectors.toList());
        dishDto.setFlavors(dishFlavorList);
        dishFlavorService.saveBatch(dishDto.getFlavors());
    }

    @Override
    @Transactional
    public void removeWithDishFlavor(Long[] ids) {
        List<Dish> dishList = this.list(Wrappers.<Dish>lambdaQuery().in(Dish::getId, (Object[]) ids));
        dishList.forEach(dish -> {
            if (Constants.DISH_ON_SALE.equals(dish.getStatus())){
                throw new BusinessException("请先下架相关菜品");
            }
        });
        //先删菜品
        this.remove(Wrappers.<Dish>lambdaQuery().in(Dish::getId, (Object[]) ids));
        //再删相关的口味
        dishFlavorService.remove(Wrappers.<DishFlavor>lambdaQuery().in(DishFlavor::getDishId, (Object[]) ids));
    }
}
