package top.lywivan.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {
    void saveWithSetmealDish(SetmealDto setmealDto);

    void deleteWithSetmealDish(Long[] ids);

    SetmealDto getByIdWithSetmealDish(Long id);

    void updateWithSetmealDish(SetmealDto setmealDto);

    List<DishDto> getDishBySetmeal(Long setmealId);
}
