package top.lywivan.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Orders;

public interface OrderService extends IService<Orders> {
    void saveWithOrderDetail(Orders orders);

    void saveAgain(Orders orders);
}