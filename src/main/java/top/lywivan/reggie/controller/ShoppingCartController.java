package top.lywivan.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.reggie.common.R;
import com.itheima.reggie.common.SecurityContext;
import com.itheima.reggie.entity.ShoppingCart;
import top.lywivan.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
@Slf4j
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 用于添加购物车，同时实现购物车中同一商品数量的改变
     * @param shoppingCart 前端传递的购物车商品
     * @return
     */
    @PostMapping("/add")
    public R add(@RequestBody ShoppingCart shoppingCart){
        Long userId = SecurityContext.getUserId();
        shoppingCart.setUserId(userId);
        shoppingCart.setCreateTime(LocalDateTime.now());
        LambdaQueryWrapper<ShoppingCart> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(ShoppingCart::getUserId,userId);
        if (shoppingCart.getDishId()!=null){
            queryWrapper.eq(ShoppingCart::getDishId,shoppingCart.getDishId());
        }else {
            queryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }
        ShoppingCart one = shoppingCartService.getOne(queryWrapper);
        if (one==null){
            shoppingCartService.save(shoppingCart);
        }else {
            one.setNumber(one.getNumber()+1);
            shoppingCartService.updateById(one);
        }
        return R.success("购物车添加成功");
    }

    /**
     * 根据当前登陆的用户，查询购物车列表
     * @return
     */
    @GetMapping("/list")
    public R list(){
        List<ShoppingCart> shoppingCartList = shoppingCartService.list(Wrappers.<ShoppingCart>lambdaQuery()
                .eq(ShoppingCart::getUserId, SecurityContext.getUserId())
                .orderByDesc(ShoppingCart::getCreateTime));
        return R.success(shoppingCartList);
    }

    /**
     * 清空当前登陆用户的购物车
     * @return
     */
    @DeleteMapping("/clean")
    public R clean(){
        shoppingCartService.remove(Wrappers.<ShoppingCart>lambdaQuery()
                .eq(ShoppingCart::getUserId,SecurityContext.getUserId()));
        return R.success("清空购物车成功");
    }

    /**
     * 用于减少购物车中的商品，注意如果购物车中的商品数量为1则直接删除该商品
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    public R sub(@RequestBody ShoppingCart shoppingCart){
        ShoppingCart one = shoppingCartService.getOne(Wrappers.<ShoppingCart>lambdaQuery()
                .eq(ShoppingCart::getUserId, SecurityContext.getUserId())
                .eq(shoppingCart.getSetmealId() != null, ShoppingCart::getSetmealId, shoppingCart.getSetmealId())
                .eq(shoppingCart.getDishId() != null, ShoppingCart::getDishId, shoppingCart.getDishId()));
        if (one.getNumber()==1){
            shoppingCartService.removeById(one.getId());
        }else {
            one.setNumber(one.getNumber()-1);
            shoppingCartService.updateById(one);
        }
        return R.success("减少商品成功");
    }
}
