package top.lywivan.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Category;
import top.lywivan.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 用于新增分类
     * @param category 前端传入的对象数据，会自动完成名称重复异常以及公共字段的填充
     * @return
     */
    @PostMapping
    public R save(@RequestBody Category category){
        categoryService.save(category);
        return R.success("添加分类成功");
    }

    /**
     * 用于分类列表的分页查询
     * @param page 当前页面
     * @param pageSize 每页显示条数
     * @return
     */
    @GetMapping("/page")
    public R page(@RequestParam(required = false,defaultValue = "1")Long page,@RequestParam(required = false,defaultValue = "10")Long pageSize){
        //分页条件：先根据sort字段值进行升序，然后根据修改日期进行降序
        LambdaQueryWrapper<Category> queryWrapper = Wrappers.<Category>lambdaQuery().orderByDesc(Category::getSort).orderByDesc(Category::getUpdateTime);
        Page<Category> result = categoryService.page(new Page<Category>(page, pageSize), queryWrapper);
        return R.success(result);
    }

    /**
     * 用于根据id删除分类
     * 当分类有关联的菜品或套餐时，要求不能删除，因此自定义了一个方法
     * @param id 前端传入的要删除的id
     * @return
     */
    @DeleteMapping
    public R delete(Long id){
        log.info("开始删除id为{}的分类",id);
        categoryService.remove(id);
        return R.success("删除成功！");
    }

    /**
     * 根据id修改分类内容
     * @param category 要修改的内容
     * @return
     */
    @PutMapping
    public R update(@RequestBody Category category){
        log.info("开始修改：{}",category);
        categoryService.updateById(category);
        return R.success("修改成功！");
    }

    /**
     * 查询某一类型的所有分类，用于下拉列表的展示
     * @param type 前端传入的分类信息
     * @return
     */
    @GetMapping("list")
    public R list(Integer type){
        List<Category> categoryList = categoryService.list(Wrappers.<Category>lambdaQuery().eq(type!=null,Category::getType, type));
        return R.success(categoryList);
    }
}
