package top.lywivan.reggie.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import top.lywivan.reggie.service.CategoryService;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类似于菜品的增删改查
 */
@RestController
@RequestMapping("/setmeal")
@Slf4j
public class SetmealController {
    @Autowired
    private SetmealService setmealService;
    @Autowired
    private CategoryService categoryService;

    @PostMapping
    @CacheEvict(value = "setmeal",key = "#setmealDto.categoryId")
    public R save(@RequestBody SetmealDto setmealDto){
        setmealService.saveWithSetmealDish(setmealDto);
        return R.success("添加套餐成功");
    }

    @GetMapping("/page")
    public R page(@RequestParam(required = false,defaultValue = "1")Long page,@RequestParam(required = false,defaultValue = "10")Long pageSize,String name){
        Page<Setmeal> setmealPage = setmealService.page(new Page<Setmeal>(page, pageSize), Wrappers.<Setmeal>lambdaQuery().like(name != null, Setmeal::getName, name).orderByDesc(Setmeal::getUpdateTime));
        Page<SetmealDto> setmealDtoPage = new Page<>();
        BeanUtils.copyProperties(setmealPage,setmealDtoPage);
        List<Setmeal> setmealList = setmealPage.getRecords();
        List<SetmealDto> setmealDtoList = setmealList.stream().map(setmeal -> {
            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(setmeal, setmealDto);
            setmealDto.setCategoryName(categoryService.getById(setmeal.getCategoryId()).getName());
            return setmealDto;
        }).collect(Collectors.toList());
        setmealDtoPage.setRecords(setmealDtoList);
        return R.success(setmealDtoPage);
    }

    @DeleteMapping
    @CacheEvict(value = "setmeal",allEntries = true)
    public R delete(Long[] ids){
        setmealService.deleteWithSetmealDish(ids);
        return R.success("删除成功！");
    }

    @GetMapping("/{id}")
    public R getById(@PathVariable("id")Long id){
        SetmealDto setmealDto = setmealService.getByIdWithSetmealDish(id);
        return R.success(setmealDto);
    }

    @PutMapping
    @CacheEvict(value = "setmeal",key = "#setmealDto.categoryId")
    public R update(@RequestBody SetmealDto setmealDto){
        setmealService.updateWithSetmealDish(setmealDto);
        return R.success("修改成功！");
    }

    @PostMapping("/status/{status}")
    public R updateStatus(@PathVariable("status")Integer status,Long[] ids){
        setmealService.update(Wrappers.<Setmeal>lambdaUpdate().set(Setmeal::getStatus,status).in(Setmeal::getId, (Object[]) ids));
        return R.success("状态更新成功");
    }

    @GetMapping("/list")
    @Cacheable(value = "setmeal",key = "#categoryId")
    public R list(Long categoryId,Integer status){
        List<Setmeal> setmealList = setmealService.list(Wrappers.<Setmeal>lambdaQuery()
                .eq(Setmeal::getCategoryId, categoryId)
                .eq(Setmeal::getStatus, status)
                .orderByDesc(Setmeal::getUpdateTime));
        return R.success(setmealList);
    }

    /**
     * 用于查看套餐详情
     * @param setmealId
     * @return
     */
    @GetMapping("/dish/{setmealId}")
    public R dish(@PathVariable("setmealId")Long setmealId){
        List<DishDto> dishDtoList = setmealService.getDishBySetmeal(setmealId);
        return R.success(dishDtoList);
    }
}
