package top.lywivan.reggie.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.reggie.common.Constants;
import com.itheima.reggie.common.R;
import com.itheima.reggie.common.SecurityContext;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/addressBook")
@Slf4j
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;

    /**
     * 查询当前用户的所有地址薄
     * @return
     */
    @GetMapping("/list")
    public R list(){
        List<AddressBook> addressBookList = addressBookService.list(Wrappers.<AddressBook>lambdaQuery()
                .eq(AddressBook::getUserId, SecurityContext.getUserId())
                .orderByDesc(AddressBook::getIsDefault)
                .orderByDesc(AddressBook::getUpdateTime));
        return R.success(addressBookList);
    }

    /**
     * 添加地址
     * @param addressBook
     * @return
     */
    @PostMapping
    public R add(@RequestBody AddressBook addressBook){
        addressBook.setUserId(SecurityContext.getUserId());
        addressBookService.save(addressBook);
        return R.success("添加地址成功");
    }

    /**
     * 给某一个地址设置为默认地址
     * @param addressBook
     * @return
     */
    @PutMapping("/default")
    public R setDefault(@RequestBody AddressBook addressBook){
        //先取消原有的默认地址
        addressBookService.update(Wrappers.<AddressBook>lambdaUpdate()
                .eq(AddressBook::getUserId,SecurityContext.getUserId())
                .set(AddressBook::getIsDefault,Constants.ADDRESS_BOOK_IS_NOT_DEFAULT));

        addressBook.setIsDefault(Constants.ADDRESS_BOOK_IS_DEFAULT);
        addressBookService.updateById(addressBook);
        return R.success("设置默认地址成功");
    }

    /**
     * 根据id查询某一地址
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id")Long id){
        AddressBook addressBook = addressBookService.getById(id);
        if (addressBook!=null){
            return R.success(addressBook);
        }else {
            return R.error("没有找到该对象");
        }
    }

    /**
     * 查询当前用户的默认地址，主要用于用户下单时进行显示
     */
    @GetMapping("default")
    public R getDefault() {
        //SQL:select * from address_book where user_id = ? and is_default = 1
        AddressBook addressBook = addressBookService.getOne(Wrappers.<AddressBook>lambdaQuery()
                .eq(AddressBook::getUserId, SecurityContext.getUserId())
                .eq(AddressBook::getIsDefault, Constants.ADDRESS_BOOK_IS_DEFAULT));

        if (null == addressBook) {
            return R.error("没有找到该对象");
        } else {
            return R.success(addressBook);
        }
    }

    /**
     * 用于C端用于修改地址信息
     * @param addressBook
     * @return
     */
    @PutMapping
    public R update(@RequestBody AddressBook addressBook){
        addressBookService.updateById(addressBook);
        return R.success("修改成功");
    }

    /**
     * 用于C端用户删除地址信息
     * @param ids
     * @return
     */
    @DeleteMapping
    public R delete(Long[] ids){
        addressBookService.remove(Wrappers.<AddressBook>lambdaQuery()
                .in(AddressBook::getId, (Object[]) ids));
        return R.success("删除地址成功");
    }
}
