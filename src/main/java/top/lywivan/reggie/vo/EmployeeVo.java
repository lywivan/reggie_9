package top.lywivan.reggie.vo;

import lombok.Data;

@Data
public class EmployeeVo {
    private Long id;

    private String username;

    private String name;

    private String phone;

    private String sex;

    private Integer status;
}
