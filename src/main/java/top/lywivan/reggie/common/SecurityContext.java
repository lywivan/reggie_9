package top.lywivan.reggie.common;

/**
 * 封装一下ThreadLocal，便于自动填充userId
 */
public class SecurityContext {
    private static ThreadLocal threadLocal=new ThreadLocal();

    public static void setUserId(Long id){
        threadLocal.set(id);
    }

    public static Long getUserId(){
        return (Long) threadLocal.get();
    }
}
