package top.lywivan.reggie.common;

public interface Constants {
    Integer DISH_ON_SALE=1;
    Integer DISH_STOP_SALE=0;
    Integer SETMEAL_ON_SALE=1;
    Integer SETMEAL_STOP_SALE=0;

    String EMPLOYEE_ID_SESSION_KEY="employee";
    String USER_ID_SESSION_KEY="user";

    String EMPLOYEE_DEFAULT_PASSWORD="123456";
    String USER_CODE_REDIS_KEY="reggie:code:";

    Integer ADDRESS_BOOK_IS_DEFAULT =1;
    Integer ADDRESS_BOOK_IS_NOT_DEFAULT =0;

    //订单状态 1待付款，2待派送，3已派送，4已完成，5已取消
    Integer ORDER_STATUS_WAIT_PAY=1;
    Integer ORDER_STATUS_WAIT_DELIVERY=2;
    Integer ORDER_STATUS_HAS_DELIVERIED=3;
    Integer ORDER_STATUS_HAS_COMPLATED=4;
    Integer ORDER_STATUS_HAS_CANCELED=5;
}
