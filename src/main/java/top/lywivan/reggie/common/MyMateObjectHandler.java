package top.lywivan.reggie.common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 利用MP实现公共字段的自动填充
 */
@Component
@Slf4j
public class MyMateObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("插入操作之前数据内容为：{}",metaObject.getOriginalObject());
        metaObject.setValue("createTime", LocalDateTime.now());
        metaObject.setValue("updateTime", LocalDateTime.now());
        //从ThreadLocal中获取userId，进行填充
        metaObject.setValue("createUser", SecurityContext.getUserId());
        metaObject.setValue("updateUser", SecurityContext.getUserId());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("修改操作之前数据内容为：{}",metaObject.getOriginalObject());
        metaObject.setValue("updateTime", LocalDateTime.now());
        metaObject.setValue("updateUser", SecurityContext.getUserId());
    }
}
