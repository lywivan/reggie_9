package top.lywivan.reggie.encrypted;

import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;

/**
 * 用于测试md5的hash算法
 */
public class MD5Test {
    @Test
    public void test1(){
        System.out.println(DigestUtils.md5DigestAsHex("123456".getBytes(StandardCharsets.UTF_8)));
    }
}
